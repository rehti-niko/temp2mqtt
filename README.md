# temp2mqtt

My scripts to send temperature and humidity information
from Raspberry Pi to Home Assistant via MQTT.

I have been using these for few years now, just backing up to public
repository.

Raspbian release Bookworm requires following addtional components

* pigpiod to be running
* python env to be creted to /home/pi/mqtt
