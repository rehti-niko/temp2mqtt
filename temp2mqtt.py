import paho.mqtt.client as mqtt
import ssl
from w1thermsensor import W1ThermSensor, Sensor
import yaml
import platform
import codecs
import json
import time
from pigpio_dht import DHT22

def get_system_id():
    name = platform.machine() + platform.system() + platform.node()
    return codecs.encode(name.encode(), 'base64').decode('ascii').strip()

systemid = get_system_id()

with open('temp2mqtt.yaml') as f:
    conf = yaml.load(f, Loader=yaml.FullLoader)

mqtt_conf = conf['mqttbroker']
client = mqtt.Client() 
client.username_pw_set(mqtt_conf['username'], mqtt_conf['password'])
client.tls_set(tls_version=ssl.PROTOCOL_TLSv1_2)
client.connect(mqtt_conf['host'], mqtt_conf['port'], 60) 

ea = conf['system']['interval']*60*2

if 'w1sensors' in conf:
  for sensor in conf['w1sensors']:
      sensorid = systemid+"-"+sensor['sensor'];
      conf_topic = 'homeassistant/sensor/'+sensorid+'/config'
      client.publish(conf_topic,payload='')
      print (conf_topic)
      conf_payload = { "device_class": "temperature",
                       "state_topic":"homeassistant/sensor/"+sensorid+"/state",
                       "name": sensor["name"],
                       "unique_id":systemid+"-"+sensor['id'],
                       "unit_of_measurement": "°C" }
      time.sleep(1)
      client.publish(conf_topic,payload=json.dumps(conf_payload), retain=True)
      print(json.dumps(conf_payload))

if 'dht22sensors' in conf:
  for sensor in conf['dht22sensors']:
      sensorid = systemid+"-"+sensor['sensor'];
      conf_topic = 'homeassistant/sensor/'+sensorid+'T/config'
      client.publish(conf_topic,payload='')
      time.sleep(1)
      conf_topich = 'homeassistant/sensor/'+sensorid+'H/config'
      client.publish(conf_topich,payload='')

      print (conf_topic)
      conf_payload = { "device_class": "temperature",
                       "state_topic":"homeassistant/sensor/"+sensorid+"/state",
                       "name": sensor["name"]+"T",
                       "unique_id":systemid+"-"+str(sensor['id'])+"T",
                       "unit_of_measurement": "°C",
                       "value_template": "{{ value_json.temperature}}" }
      client.publish(conf_topic,payload=json.dumps(conf_payload), retain=True)
      print(json.dumps(conf_payload))

      conf_topic = 'homeassistant/sensor/'+sensorid+'H/config'
      #client.publish(conf_topic,payload='')
      print (conf_topic)
      conf_payload = { "device_class": "humidity",
                       "state_topic":"homeassistant/sensor/"+sensorid+"/state",
                       "name": sensor["name"]+"H",
                       "unique_id":systemid+"-"+str(sensor['id'])+"H",
                       "unit_of_measurement": "%",
                       "value_template": "{{ value_json.humidity}}" }
      client.publish(conf_topic,payload=json.dumps(conf_payload), retain=True)
      print(json.dumps(conf_payload))
    
client.disconnect()

while True:
    client.connect(mqtt_conf['host'], mqtt_conf['port'], 60)
    if 'w1sensors' in conf:
      for sensor in conf['w1sensors']:
          sensorid = systemid+"-"+sensor['sensor'];
          state_topic = "homeassistant/sensor/"+sensorid+"/state"
          w1sensor = W1ThermSensor(Sensor.DS18B20, sensor['id'])
          temperature = round(w1sensor.get_temperature(),1)
          client.publish(state_topic, payload=temperature)
          print(state_topic + " --> " + str(temperature))

    if 'dht22sensors' in conf:
      for sensor in conf['dht22sensors']:
          temperature=None
          humidity=None
          sensorid = systemid+"-"+sensor['sensor'];
          state_topic = "homeassistant/sensor/"+sensorid+"/state"
          dht_device = DHT22(sensor['id'])
          print(dht_device)
          ti = 0;
          while ti < 10:
            try:
              #print("Attempting to read DHT22 temperature")
              result = dht_device.read()
              print(result)
              humidity = result['humidity']
              temperature = result['temp_c'] 
              break
            except:
              print("Failed to get temperature or humidity, retry...")
              ti+=1
              time.sleep(2)
          if ( temperature is not None and humidity is not None and 
               -100 < temperature < 200 and 0 < humidity < 110 ):
            payload = { "temperature": temperature, "humidity": humidity }
            client.publish(state_topic, payload=json.dumps(payload))
            print(state_topic + " --> " + str(temperature) + "/" + str(humidity))
          else:
            print("Invalid values --> " + str(temperature) + "/" + str(humidity))
      client.disconnect()

    time.sleep(conf['system']['interval']*60)
